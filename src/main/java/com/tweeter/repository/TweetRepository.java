package com.twitter.repository;

import com.twitter.model.Tweet;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface TweetRepository extends CrudRepository<Tweet, UUID> {
}
