package com.twitter.repository;

import com.twitter.model.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface UserRepository extends CrudRepository<User, UUID> {
    List<User> findAllByEmailIsLike(String term);
//    List<User> findAllByEmailIsLikeOrNicknameLikeOrFirstNameLikeOrLastNameLikeOrPhoneLike(String term);
}
