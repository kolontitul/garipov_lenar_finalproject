package com.twitter.service;

import com.twitter.model.Tweet;

import java.util.List;
import java.util.UUID;

public interface TweetService {
    Tweet getById(UUID id);
    List<Tweet> getAll();
    Tweet createTweet(Tweet tweet);
    Tweet updateTweet(UUID id, Tweet tweet);
    void delete(UUID id);
}
